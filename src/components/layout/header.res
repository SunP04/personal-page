@react.component
let make = (~email, ~linkedin, ~gitlab) => {
  open Common

  <header
    className="w-full max-w-lg rounded-xl shadow-lg shadow-black px-10 py-2 bg-yellow-400 text-gray-800">
    <main className="w-full h-full flex flex-row justify-between items-center">
      <h1 className="font-bold font-rstorm text-accent text-2xl">
        <Text> "Sun" </Text>
      </h1>
      <div id="button-area" className="flex flex-row space-x-5 items-center">
        <Link href={"mailto:" ++ email} type_="external">
          <img src="/icons/email.svg" alt="email-icon" className="w-7 h-7" />
        </Link>
        <Link href=gitlab type_="external">
          <img src="/icons/gitlab.svg" alt="gitlab-icon" />
        </Link>
        <Link href=linkedin type_="external">
          <img src="/icons/linkedin.svg" alt="linked-icon" className="w-8 h-8" />
        </Link>
      </div>
    </main>
  </header>
}

let default = make
