module Text = {
  @react.component
  let make = (~children: string) => {
    <> {children->React.string} </>
  }
}

module For = {
  @react.component
  let make = (~items: array<'a>, ~children: 'a => React.element) => {
    items->Array.map(children)->React.array
  }
}
