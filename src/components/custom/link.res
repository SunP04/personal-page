@react.component
let make = (~className="", ~href, ~type_, ~children) => {
  <a
    href={href}
    className={`${className} cursor-pointer hover:text-blue-700 hover:underline`}
    target={type_ == "external" ? "_blank" : "_self"}
    rel={type_ == "external" ? "noopener" : ""}>
    {children}
  </a>
}

let default = make
