type t = {
  quotes: array<string>,
  author: string,
}

@react.component
let make = (~quote) => {
  open Common

  <div className="bg-white rounded-lg shadow-lg p-6 max-w-sm">
    <h3 className="text-xl font-bold mb-4">
      <Text> {quote.author} </Text>
    </h3>
    <ul className="space-y-1">
      <For items=quote.quotes>
        {quote =>
          <li key={quote} className="text-gray-700">
            <div className="border-l-3xl border-red-800 text-sm italic">
              <Text> {"\"" ++ quote ++ "\""} </Text>
            </div>
          </li>}
      </For>
    </ul>
  </div>
}

let default = make
