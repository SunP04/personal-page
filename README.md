# Sun - Personal Page

This is my personal page, used to present myself to other people.

This project is focused always on simplicity, using only AstroJS (previously was used with HTML + WebComponents).

## Running the project:

Since this is an astro project, you need to first install the npm dependencies.

Use `npm/bun/pnpm install` to install the dependencies.

Then, you can just run the application in either:

- Dev Mode: `npm run dev`
- Production: `npm run build && npm run preview`

## Deploying:

This project is using astro, so it already provides CLI utilities to bundle the application.

Use `npm run build` to build the application, then you can just publish the created folder.

## License:

This project is under MIT License which can be seen [here](LICENSE).
